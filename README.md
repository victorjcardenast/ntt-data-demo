# Demo NTT DATA



## Proyecto Demo NTT DATA

Proyecto Demo para NTT DATA 

### Este proyecto tiene como objetivo mostrar la funcionalidad de Sprig Boot
### Utilitarios previos
```
# Con JDK 11 minimo
# Base de datos Postgres V 11 minimo
# Intellij
# Utiliza Gitlab como repositorio 
# Postman
```
## Pasos
### 1. Creacion credenciales Base de Datos
Este demo utiliza Postgres V 11
```
1. Creamos usuario nttdata con clave nttdata y con privilegios creacion DB
2. Creamos base de datos nttdatadb y atamos al nuevo usuario
```

### 2. Clonamos el repositorio en el directorio a su eleccion con el comando
```
cd directorioproyecto
Abrimos gitbash
git init
git clone https://gitlab.com/victorjcardenast/ntt-data-demo.git
```
### Running the Application

Once you checked out the project run Maven Install:

```
mvn clean install
```~~~~
Create your docker image running the command below:

```
docker build -t "spring-boot-docker" .
```

Run your docker container using this command:

```
docker run --name spring-boot-docker -p 8080:8080 spring-boot-docker:latest
```
### 3. Ejecutamos Intellij y abrimos el proyecto
Esperamos a que MAVEN actualice el proyecto

Ejecutamos el archivo: DemoApplication.java

## Api Rest
### Clientes
```
```
Estados Genero: Masculino, Femenino
DNI: maximo 13 caracteres
URL: http://localhost:8080/api/clientes/create
Metodo: POST
Body:
{
    "password": "1234",
    "state": true,
    "dni": "1757560510",
    "names": "Jose",
    "surnames": "Lema",
    "birthday":"1993-01-26T00:00:00.000Z",
    "address": "Otavalo sn y principal",
    "phone": "098254785",
    "gender": "Femenino"
}
```
```
Estados Genero: Masculino, Femenino
DNI: maximo 13 caracteres
URL: http://localhost:8080/api/clientes/update
Metodo: PUT
Body:
{
"id": 1,
"password": "1234",
"state": true,
"dni": "1757560511",
"names": "Jose Israel",
"surnames": "Lema",
"birthday":"1993-01-26T00:00:00.000Z",
"address": "Otavalo sn y principal",
"phone": "098254785",
"gender": "Femenino"
}
```
```
URL: http://localhost:8080/api/clientes/delete/1
Metodo: DELETE
```
### Cuentas
```
accountType: Ahorros, Corriente
URL: http://localhost:8080/api/cuentas/create
Metodo: POST
Body:
{
    "accountNumber": "478758",
    "accountType": "Ahorro",
    "initialBalance": 2000.00,
    "state": true,
    "clients": [
        {"id": 1}
    ]
}
```

### Movimientos
```
transactionType: Retiro, Deposito
URL: http://localhost:8080/api/movimientos/create
Metodo: POST
Body:
{
    "transactionType": "Retiro",
    "accountNumber": "1234564",
    "amount": 10.00
}
```
```
Reporte
URL: http://localhost:8080/api/movimientos/report?accountNumber=1234563&fchIn=15/11/2022&fchOut=16/11/2022
Metodo: GET
Body:
```
## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Víctor Cárdenas** - [victor-cardenas](https://gitlab.com/victorjcardenast)