CREATE SCHEMA nttdata AUTHORIZATION nttdata;

CREATE TABLE nttdata.clients (
	id bigserial NOT NULL,
	address varchar(100) NOT NULL,
	birthday timestamp NOT NULL,
	dni varchar(13) NOT NULL,
	gender varchar(255) NULL,
	names varchar(50) NOT NULL,
	phone varchar(255) NULL,
	surnames varchar(50) NOT NULL,
	"password" varchar(20) NOT NULL,
	state bool NOT NULL DEFAULT true,
	CONSTRAINT clients_pkey PRIMARY KEY (id),
	CONSTRAINT uk_7lb86p1hnr0kauij7t55krebo UNIQUE (dni)
);

CREATE TABLE nttdata.accounts (
	id bigserial NOT NULL,
	account_number varchar(255) NOT NULL,
	account_type varchar(255) NULL,
	initial_balance float8 NULL,
	state bool NOT NULL DEFAULT true,
	CONSTRAINT accounts_pkey PRIMARY KEY (id),
	CONSTRAINT uk_6kplolsdtr3slnvx97xsy2kc8 UNIQUE (account_number)
);

CREATE TABLE nttdata.account_client (
	account_id int8 NOT NULL,
	client_id int8 NOT NULL,
	CONSTRAINT account_client_pkey PRIMARY KEY (account_id, client_id),
	CONSTRAINT fk9e3w3jri3gh4i09ow7qx6mpyq FOREIGN KEY (account_id) REFERENCES nttdata.accounts(id),
	CONSTRAINT fkjnkly273fbs4k8yqhs6bkhtl7 FOREIGN KEY (client_id) REFERENCES nttdata.clients(id)
);

CREATE TABLE nttdata.transactions (
	id bigserial NOT NULL,
	amount float8 NULL,
	balance float8 NULL,
	transaction_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	transaction_type varchar(255) NULL,
	account_id int8 NULL,
	initial bool NULL,
	CONSTRAINT transactions_pkey PRIMARY KEY (id),
	CONSTRAINT fk20w7wsg13u9srbq3bd7chfxdh FOREIGN KEY (account_id) REFERENCES nttdata.accounts(id)
);

