package com.nttdata.demo.modules.account.controller;

import com.nttdata.demo.modules.account.entity.Account;
import com.nttdata.demo.modules.account.service.AccountService;
import com.nttdata.demo.utils.error.dto.ErrorDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "api/cuentas",
        produces = MediaType.APPLICATION_JSON_VALUE)
public class AccountController {

    @Autowired
    private AccountService accountService;

    @PostMapping(value = "/create")
    public ResponseEntity<?> create(@RequestBody Account account) {

        try{
            return ResponseEntity.status(HttpStatus.CREATED).body(accountService.save(account));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ErrorDto("Key account_number already exists.",
                            "SQLState: 23505", "failed"));
        }

    }

    @PutMapping(value = "/update")
    public ResponseEntity<?> update(@RequestBody Account account) {

        try{
            return ResponseEntity.status(HttpStatus.CREATED).body(accountService.save(account));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ErrorDto("Key account_number already exists.",
                            "SQLState: 23505", "failed"));
        }

    }

    @DeleteMapping(value = "/delete/{accountNumber}")
    public ResponseEntity<?> create(@PathVariable String accountNumber) {

        try {
            if (accountService.deleteByAccountNumber(accountNumber)) {
                return ResponseEntity.status(HttpStatus.OK).body(
                        new ErrorDto("Cuenta eliminada",
                                "200", "ok")
                );
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ErrorDto("Error al eliminar cuenta",
                                "error delete", "failed")
                );
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ErrorDto("Error al eliminar cuenta",
                            "error delete", "failed")
            );
        }

    }
}
