package com.nttdata.demo.modules.account.service;


import com.nttdata.demo.modules.account.entity.Account;

import java.util.Optional;

public interface AccountService {

    public Iterable<Account> findAll();

    public Optional<Account> findById(Long id);

    public Account save(Account account);

    public Account update(Account account);

    public Boolean delete(Long id);

    public Boolean deleteByAccountNumber(String accountNumber);
}
