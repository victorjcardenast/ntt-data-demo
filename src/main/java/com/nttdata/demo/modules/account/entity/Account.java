package com.nttdata.demo.modules.account.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nttdata.demo.modules.account.enums.AccountType;
import com.nttdata.demo.modules.client.entity.Client;
import com.nttdata.demo.modules.transaction.entity.Transaction;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "accounts", schema = "nttdata")
public class Account implements Serializable {

    private static final long serialVersionUID = 1514787347921293655L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "account_number", nullable = false, unique = true)
    private String accountNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "account_type")
    private AccountType accountType;

    @Column(name = "initial_balance")
    private Double initialBalance;

    @Column(columnDefinition = "BOOLEAN NOT NULL DEFAULT TRUE")
    private Boolean state;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(
            name = "account_client",
            joinColumns = { @JoinColumn(name = "account_id") },
            inverseJoinColumns = { @JoinColumn(name = "client_id") }
    )
    @JsonIgnoreProperties(value = "accounts", allowSetters = true)
    private Set<Client> clients = new HashSet<>();

    @OneToMany(
            mappedBy = "account",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnoreProperties(value = "account", allowSetters = true)
    private List<Transaction> transactions;

    public Account() {
    }

    public void addClient(Client client) {
        this.clients.add(client);
        client.getAccounts().add(this);
    }

    public void removeClient(Client client) {
        this.clients.remove(client);
        client.getAccounts().remove(this);
    }

//    public void setAddresses(List<Client> clients) {
//        this.clients.clear();
//        clients.forEach(this::addClient);
//    }

}
