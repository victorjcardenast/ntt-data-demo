package com.nttdata.demo.modules.account.service;

import com.nttdata.demo.modules.account.entity.Account;
import com.nttdata.demo.modules.account.repository.AccountRepository;
import com.nttdata.demo.modules.client.entity.Client;
import com.nttdata.demo.modules.client.repository.ClientRepository;
import com.nttdata.demo.modules.client.repository.PersonRepository;
import com.nttdata.demo.modules.transaction.entity.Transaction;
import com.nttdata.demo.modules.transaction.enums.TransactionType;
import com.nttdata.demo.modules.transaction.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public Iterable<Account> findAll() {
        return null;
    }

    @Override
    public Optional<Account> findById(Long id) {
        return Optional.empty();
    }

    @Override
    @Transactional
    public Account save(Account account) {

        Set<Client> clients = account.getClients();
        Account accountDb = account;
        accountDb.setClients(new HashSet<>());

        for (Client client : clients) {
            Client clientFind = clientRepository.findById(client.getId()).get();
            accountDb.addClient(clientFind);
        }
        accountDb = accountRepository.save(accountDb);

        Transaction transaction = new Transaction(
                TransactionType.Deposito
                , accountDb.getInitialBalance()
                , accountDb.getInitialBalance()
                , accountDb
                , true
        );
        transaction = transactionRepository.save(transaction);
        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction);

        accountDb.setTransactions(transactions);
        return accountDb;
    }

    @Override
    @Transactional
    public Account update(Account account) {
        Set<Client> clients = account.getClients();
        Account accountDb = accountRepository.findById(account.getId()).get();
        accountDb.setState(account.getState());
        accountDb.setAccountType(account.getAccountType());
        accountDb.setAccountNumber(account.getAccountNumber());
        accountDb.setInitialBalance(account.getInitialBalance());
        accountDb.setClients(new HashSet<>());

        for (Client client : clients) {
            Client clientFind = clientRepository.findById(client.getId()).get();
            accountDb.addClient(clientFind);
        }
        accountDb = accountRepository.save(accountDb);
        return accountDb;
    }

    @Override
    public Boolean delete(Long id) {
        Account account = accountRepository.findById(id).get();
        account.setState(false);
        accountRepository.save(account);
        return true;
    }

    @Override
    public Boolean deleteByAccountNumber(String accountNumber) {
        Account account = accountRepository.findByAccountNumber(accountNumber).get();
        account.setState(false);
        accountRepository.save(account);
        return true;
    }
}
