package com.nttdata.demo.modules.account.enums;

public enum AccountType {
    Ahorros,
    Corriente
}
