package com.nttdata.demo.modules.client.controller;

import com.nttdata.demo.modules.client.service.ClientService;
import com.nttdata.demo.modules.client.entity.Client;
import com.nttdata.demo.utils.error.ErrorClient;
import com.nttdata.demo.utils.error.dto.ErrorDto;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "api/clientes",
        produces = MediaType.APPLICATION_JSON_VALUE)
public class ClientesController {

    @Autowired
    private ClientService clientService;

    @PostMapping(value = "/create")
    public ResponseEntity<?> create(@RequestBody Client client) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(clientService.save(client));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ErrorDto("Key dni already exists",
                            "SQLState: 23505", "failed")
            );
        }
    }

    @PutMapping(value = "/update")
    public ResponseEntity<?> update(@RequestBody Client client) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(clientService.save(client));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ErrorDto("Error al actualizar",
                            "Error update", "failed")
            );
        }
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        try {
            if (clientService.delete(id)) {
                return ResponseEntity.status(HttpStatus.OK).body(
                        new ErrorDto("Cliente eliminado",
                                "200", "ok")
                );
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ErrorDto("Error al eliminar cliente",
                                "error delete", "failed")
                );
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ErrorDto("Error al eliminar cliente",
                            "error delete", "failed")
            );
        }
    }
}
