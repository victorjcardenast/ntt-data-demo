package com.nttdata.demo.modules.client.service;


import com.nttdata.demo.modules.client.entity.Client;

import java.util.Optional;

public interface ClientService {

    public Iterable<Client> findAll();

    public Optional<Client> findById(Long id);

    public Client save(Client client);

    public Boolean delete(Long id);

    public void deleteById(Long id);
}
