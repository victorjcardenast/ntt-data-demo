package com.nttdata.demo.modules.client.enums;

public enum Gender {
    Masculino,
    Femenino
}
