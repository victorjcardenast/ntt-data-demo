package com.nttdata.demo.modules.client.service;

import com.nttdata.demo.modules.client.entity.Client;
import com.nttdata.demo.modules.client.repository.ClientRepository;
import com.nttdata.demo.modules.client.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public Iterable<Client> findAll() {
        return null;
    }

    @Override
    public Optional<Client> findById(Long id) {
        return Optional.empty();
    }

    @Override
    @Transactional
    public Client save(Client client) {
        Client clientAux = client;
        return clientRepository.save(clientAux);
    }

    @Override
    public Boolean delete(Long id) {
        Client client = clientRepository.findById(id).get();
        client.setState(false);
        clientRepository.save(client);
        return true;
    }

    @Override
    public void deleteById(Long id) {

    }
}
