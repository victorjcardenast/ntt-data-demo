package com.nttdata.demo.modules.client.repository;

import com.nttdata.demo.modules.client.entity.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {
}
