package com.nttdata.demo.modules.client.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nttdata.demo.modules.account.entity.Account;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "clients", schema = "nttdata")
public class Client  extends Person {

    @Column(length = 20, nullable = false)
    private String password;

    @Column(columnDefinition = "BOOLEAN NOT NULL DEFAULT TRUE")
    private Boolean state;

    @ManyToMany(mappedBy = "clients")
    @JsonIgnoreProperties(value = "clients", allowSetters = true)
    private Set<Account> accounts  = new HashSet<>();

    public Client() {

    }


}
