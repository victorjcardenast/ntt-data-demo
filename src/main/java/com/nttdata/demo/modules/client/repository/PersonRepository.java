package com.nttdata.demo.modules.client.repository;

import com.nttdata.demo.modules.client.entity.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {
}
