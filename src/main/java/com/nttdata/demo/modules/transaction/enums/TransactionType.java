package com.nttdata.demo.modules.transaction.enums;

public enum TransactionType {
    Retiro,
    Deposito
}
