package com.nttdata.demo.modules.transaction.service;

import com.nttdata.demo.modules.account.entity.Account;
import com.nttdata.demo.modules.account.repository.AccountRepository;
import com.nttdata.demo.modules.transaction.entity.Transaction;
import com.nttdata.demo.modules.transaction.entity.TransactionDTO;
import com.nttdata.demo.modules.transaction.enums.TransactionType;
import com.nttdata.demo.modules.transaction.repository.TransactionRepository;
import com.nttdata.demo.utils.error.dto.ErrorDto;
import com.nttdata.demo.utils.error.dto.TransactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Iterable<Transaction> findAll() {
        return null;
    }

    @Override
    public Optional<Transaction> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public ErrorDto save(TransactionDto transaction) {
//        this.accountStatus(transaction.getAccountNumber(), new Date(), new Date());

        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-DD");
        String das = sdf.format(new Date());
        Transaction transactionDB = new Transaction();
        Account account = accountRepository.findByAccountNumber(transaction.getAccountNumber()).get();
        Double balance = transactionRepository.lastBalance(account.getAccountNumber());

        ErrorDto error = new ErrorDto();
        switch (transaction.getTransactionType().toLowerCase()) {
            case "retiro":
                if (transactionRepository.validateInitialAmount(account.getAccountNumber())) {
                    if (balance >= transaction.getAmount()) {
                        transactionDB = new Transaction(
                                TransactionType.Retiro
                                , balance - transaction.getAmount()
                                , transaction.getAmount()
                                , account
                        );
                        transactionDB = transactionRepository.save(transactionDB);

                        error = new ErrorDto(
                                "Transaccion realizada con exito" + transactionDB.getId()
                                , "200"
                                , "ok"
                        );
                    } else {
                        error = new ErrorDto(
                                "Monto excede disponible"
                                , "403"
                                , "failed"
                        );
                    }
                }
                if (!transactionRepository.validateInitialAmount(account.getAccountNumber())) {
                    if (balance >= transaction.getAmount()) {
                        transactionDB = new Transaction(
                                TransactionType.Retiro
                                , balance - transaction.getAmount()
                                , transaction.getAmount()
                                , account
                                , false
                        );

                        transactionDB = transactionRepository.save(transactionDB);
                        error = new ErrorDto(
                                "Transaccion realizada con exito" + transactionDB.getId()
                                , "200"
                                , "ok"
                        );
                    } else {
                        error = new ErrorDto(
                                "Monto excede disponible"
                                , "403"
                                , "failed"
                        );
                    }
                } else {
                    error = new ErrorDto(
                            "Cupo diario excedido"
                            , "403"
                            , "failed"
                    );

                }

                break;
            case "deposito":
                transactionDB = new Transaction(
                        TransactionType.Deposito
                        , balance + transaction.getAmount()
                        , transaction.getAmount()
                        , account
                        , false
                );

                transactionDB = transactionRepository.save(transactionDB);
                error = new ErrorDto(
                        "Transaccion realizada con exito: " + transactionDB.getId()
                        , "200"
                        , "ok"
                );
                break;
            default:
                error = new ErrorDto(
                        "Tipo transaccion no encontrada"
                        , "403"
                        , "failed"
                );
                break;
        }

        return error;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public List<TransactionDTO> accountStatus(String  accountNumber, String initDate, String endDate) {
        List<TransactionDTO> transactions = new ArrayList<>();

        try {
            Date addDate = new SimpleDateFormat("dd/MM/yyyy").parse(endDate);
            Calendar c = Calendar.getInstance();
            c.setTime(addDate);
            c.add(Calendar.DATE, 1);
            addDate = c.getTime();

//            String newDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(addDate);

            transactions = transactionRepository.accountStatus(
                    accountNumber
                    , new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(initDate + " 00:00:00")
                    , new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(
                            new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(addDate)
                    )
            );
        } catch (ParseException e) {
             System.out.println(e);
        }

        return transactions;
    }
}
