package com.nttdata.demo.modules.transaction.service;

import com.nttdata.demo.modules.transaction.entity.Transaction;
import com.nttdata.demo.modules.transaction.entity.TransactionDTO;
import com.nttdata.demo.utils.error.dto.ErrorDto;
import com.nttdata.demo.utils.error.dto.TransactionDto;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface TransactionService {

    public Iterable<Transaction> findAll();

    public Optional<Transaction> findById(Long id);

    public ErrorDto save(TransactionDto transaction);

    public void deleteById(Long id);

    public List<TransactionDTO> accountStatus(String  accountNumber, String initDate, String endDate) throws ParseException;
}
