package com.nttdata.demo.modules.transaction.repository;

import com.nttdata.demo.modules.transaction.entity.Transaction;
import com.nttdata.demo.modules.transaction.entity.TransactionDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Date;
import java.util.List;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    @Query(value = "select case when cast((select u.value from utils u where u.name='limite diario de retiro') as float) " +
            "> sum(t.amount) " +
            "then true else false end " +
            "from transactions t inner join accounts a on a.id = t.account_id " +
            "where cast(t.transaction_date as date) = (current_date) " +
            " and t.transaction_type ='Retiro' " +
            "and a.account_number=:accountNumber "
            , nativeQuery = true)
    public Boolean validateDailyAmount(@Param("accountNumber") String accountNumber);

    @Query(value = "select t.initial from transactions t where t.id=(" +
            "select max(t.id) from transactions t inner join accounts a on a.id = t.account_id " +
            "where a.account_number=:accountNumber)"
            , nativeQuery = true)
    public Boolean validateInitialAmount(@Param("accountNumber") String accountNumber);

    @Query(value = "select t.balance from transactions t where t.id=(" +
            "select max(t.id) from transactions t inner join accounts a on a.id = t.account_id " +
            "where a.account_number=:accountNumber)"
            , nativeQuery = true)
    public Double lastBalance(@Param("accountNumber") String accountNumber);

    @Query(value = "SELECT " +
            " new com.nttdata.demo.modules.transaction.entity.TransactionDTO(" +
            " t.transactionDate, " +
            " c.names || ' ' || c.surnames, " +
            " t.account.accountNumber, " +
            " cast(t.account.accountType as string)," +
            " t.account.initialBalance, " +
            " t.amount, " +
            " t.balance, " +
            "  cast(t.transactionType as string) " +
            ") " +
            "FROM Transaction t join t.account a left join a.clients c "
            + "WHERE t.account.accountNumber=:accountNumber "
           + " AND cast(t.transactionDate as date) between :initDate AND :endDate "
    )
    public List<TransactionDTO> accountStatus(
             @PathVariable("accountNumber") String accountNumber
             , @PathVariable("initDate") Date initDate, @PathVariable("endDate") Date endDate
    );
}
