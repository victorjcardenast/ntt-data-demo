package com.nttdata.demo.modules.transaction.controller;

import com.nttdata.demo.modules.transaction.entity.Transaction;
import com.nttdata.demo.modules.transaction.entity.TransactionDTO;
import com.nttdata.demo.modules.transaction.service.TransactionService;
import com.nttdata.demo.utils.error.dto.ErrorDto;
import com.nttdata.demo.utils.error.dto.TransactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "api/movimientos",
        produces = MediaType.APPLICATION_JSON_VALUE)
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @PostMapping(value = "/create")
    public ResponseEntity<?> create(@RequestBody TransactionDto transaction) {
        ErrorDto error = transactionService.save(transaction);

        if (!error.getCode().equals("403")) {
            return ResponseEntity.status(HttpStatus.CREATED).body(error);
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(error);
        }
    }

    @GetMapping(value = "/report")
    public ResponseEntity<?> report(@RequestParam("accountNumber") String accountNumber,
                                    @RequestParam("fchIn") String fchIn,
                                    @RequestParam("fchOut") String fchOut) {
        List<TransactionDTO> error = null;
        try {
            error = transactionService.accountStatus(accountNumber.toString(), fchIn, fchOut);
        } catch (ParseException e) {
            e.printStackTrace();
        }

//        if (!error.getCode().equals("403")) {
            return ResponseEntity.status(HttpStatus.OK).body(error);
//        } else {
//            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(error);
//        }
    }
}
