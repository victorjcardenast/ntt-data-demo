package com.nttdata.demo.modules.transaction.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nttdata.demo.modules.account.entity.Account;
import com.nttdata.demo.modules.transaction.enums.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "transactions", schema = "nttdata")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP")
    private Date transactionDate = new Date();

    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_type")
    private TransactionType transactionType;

    @Column
    private Double balance;

    @Column
    private Double amount;

    @Column
    private Boolean initial;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    @JsonIgnoreProperties(value = "transactions")
    private Account account;

    public Transaction() {
    }

    public Transaction(TransactionType transactionType, Double amount, Account account) {
        this.transactionType = transactionType;
        this.amount = amount;
        this.account = account;
    }

    public Transaction(
            TransactionType transactionType
            , Double balance
            , Double amount
            , Account account
            ,Boolean initial) {
        this.transactionDate = new Date();
        this.transactionType = transactionType;
        this.balance = balance;
        this.amount = amount;
        this.account = account;
        this.initial = initial;
    }

    public Transaction(
            TransactionType transactionType
            , Double balance
            , Double amount
            , Account account) {
        this.transactionDate = new Date();
        this.transactionType = transactionType;
        this.balance = balance;
        this.amount = amount;
        this.account = account;
        this.initial = false;
    }


}
