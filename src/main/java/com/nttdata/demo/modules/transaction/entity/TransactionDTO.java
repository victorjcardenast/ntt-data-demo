package com.nttdata.demo.modules.transaction.entity;

import com.nttdata.demo.modules.client.entity.Client;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
public class TransactionDTO {

    private Date transactionDate;
    private String accountNumber;
    private String accountType;
    private Double initialBalance;
    private Double amount;
    private Double balance;
    private String names;
    private String transactionType;

    public TransactionDTO(){}

    public TransactionDTO(
            Date transactionDate,
            String accountNumber,
            String accountType,
            Double initialBalance,
            Double amount,
            Double balance) {
        this.transactionDate = transactionDate;
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.initialBalance = initialBalance;
        this.amount = amount;
        this.balance = balance;
    }

    public TransactionDTO(
            Date transactionDate,
            String clients,
            String accountNumber,
            String accountType,
            Double initialBalance,
            Double amount,
            Double balance) {
        this.transactionDate = transactionDate;
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.initialBalance = initialBalance;
        this.amount = amount;
        this.balance = balance;
        this.names = clients;

    }

    public TransactionDTO(
            Date transactionDate,
            String clients,
            String accountNumber,
            String accountType,
            Double initialBalance,
            Double amount,
            Double balance,
            String transactionType) {
        this.transactionDate = transactionDate;
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.initialBalance = initialBalance;
        this.amount = amount;
        this.balance = balance;
        this.names = clients;
        this.transactionType = transactionType;
    }

}
