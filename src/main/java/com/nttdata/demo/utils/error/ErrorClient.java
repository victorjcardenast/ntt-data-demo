package com.nttdata.demo.utils.error;

public class ErrorClient extends Exception {
    public ErrorClient(String msg) {
        super(msg);
    }
}
